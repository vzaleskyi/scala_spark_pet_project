#Functional Programing in Scala Capstone
## Description

### First Milestone
<p>
The first milestone consists in extracting meaningful information from the dataset. The methods to implement live in the `Extraction.scala`.
</p>

You are given several .csv files containing two kinds of data:

- Weather station’s locations (stations.csv file) ;
- Temperature records for a year (files 1975.csv, 1976.csv, etc.).

Dataset can be found here: http://alaska.epfl.ch/files/scala-capstone-data.zip
<p>
Your goal is to merge the data from these sources to get a series of information of the form date × location × temperature.
</p>

### Second Milestone
This milestone consists in producing images showing the content of the temperature records. You will have to complete the file `Visualization.scala`. 

<p>
Your records contain the average temperature over a year, for each station’s location. 
Your work consists in building an image of 360×180 pixels, where each pixel shows the temperature at its location. 
The point at latitude 0 and longitude 0 (the intersection between the Greenwich meridian and the equator) will be at the center of the image<br>
Scala image processing library: https://index.scala-lang.org/sksamuel/scrimage/scrimage-core/2.1.8?target=_2.12
</p>

<p>
You will have to spatially interpolate the data in order to guess the temperature corresponding to the location of each pixel.<br>
Inverse distance weighting: https://en.wikipedia.org/wiki/Inverse_distance_weighting<br>
Great-circle distance: https://en.wikipedia.org/wiki/Great-circle_distance
</p>

<p>
Then you will have to convert this temperature value into a pixel color based on a color scale.
For temperatures between thresholds, say, between 12°C and 32°C, you will have to compute a linear interpolation between the yellow and red colors.<br>
Linear interpolation: https://en.wikipedia.org/wiki/Linear_interpolation
</p>

### Third Milestone
<p>
This milestone consists in producing images compatible with most Web based map visualization tools, so that you can see your data in an interactive Web page. 
You will have to complete the file `Interaction.scala`.
</p>

<p>
In Web based mapping applications, the whole map is broken down into small images of size 256×256 pixels, called tiles. 
Each tile shows a part of the map at a given location and zoom level. 
Your work consists in producing these tiles using the Web Mercator projection.<br>
Web Mercator: https://en.wikipedia.org/wiki/Web_Mercator
</p>

<p>
To describe the position and size of tiles, we need to introduce the tile coordinate system, which is composed of an x value, a y value and a zoom level.<br>
http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#X_and_Y
</p>

<p>
The tile shows the given temperatures, using the given color scale, at the location corresponding to the given zoom, x and y values. 
Note that the pixels of the image must be a little bit transparent so that when we will overlay the tile on the map, the map will still be visible. 
We recommend using an alpha value of 127.
</p>

<p>
A simple way to compute the corresponding latitude and longitude of each pixel within a tile
is to rely on the fact that each pixel in a tile can be thought of a subtile at a higher zoom level (256 = 2⁸).<br>
http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Subtiles
</p>

<p>
Then you need to generate all the tiles for a given dataset yearlyData, for zoom levels 0 to 3 (included). 
The dataset contains pairs of (Year, Data) values, or, said otherwise, data associated with years. 
In your case, this data will be the result of Extraction.locationYearlyAverageRecords. 
The second parameter of the generateTiles method is a function that takes a year, the coordinates of the tile to generate, and the data associated with the year, and computes the tile and writes it on your filesystem.
</p>

### Fourth Milestone
<p>
One of the primary goals of this project is to be able to visualize the evolution of the climate. 
That’s why we propose to visualize the deviations of the temperatures over the years, rather than just the temperatures themselves. 
The goal of this milestone is to compute such deviations. 
You will have to complete the file `Manipulation.scala`. 
</p>

<p>
Computing deviations means comparing a value to a previous value which serves as a reference, or a “normal” temperature. 
You will first compute the average temperatures all over the world between 1975 and 1990. This will constitute your reference temperatures, which we refer to as “normals”. 
You will then compare the yearly average temperatures, for each year between 1991 and 2015, to the normals.
</p>

<p>
In order to make things faster, you will first spatially interpolate your scattered data into a regular grid.
Once you will have such a grid for each year, you will easily be able to compute average (coordinate wise) over years and deviations.
</p>

### Fifth Milestone
<p>
The goal of this milestone is to produce tile images from the grids generated at the previous milestone. 
You will have to complete the file `Visualization2.scala`.
</p>

<p>
As in the 3rd milestone, you will have to compute the color of every pixel of the tiles. 
But now the situation has changed: 
instead of working with a set of scattered points, you have a regular grid of points.
</p>

<p>
You can leverage the grid to use a faster interpolation algorithm: 
you can now use bilinear interpolation rather than inverse distance weighting.<br>
https://en.wikipedia.org/wiki/Bilinear_interpolation#Unit_Square
</p>

<p>
In this form of bilinear interpolation, the location of the point to estimate is given by coordinates x and y, which are numbers between 0 and 1. 
The algorithm considers that the four known points, d00, d01, d10 and d11, form a unit square whose origin is its top-left corner. 
As such, the coordinates of a pixel inside of a grid cell can be described by the following case class, defined in `models.scala`: case class CellPoint.
</p>

Once you have implemented the milestone methods, you are ready to generate the tiles showing the deviations for all the years between 1990 and 2015, so that the final application (in the last milestone) will nicely display them:

- Compute normals from yearly temperatures between 1975 and 1990 ;
- Compute deviations for years between 1991 and 2015 ;
- Generate tiles for zoom levels going from 0 to 3, showing the deviations. Use the output method of Image to write the tiles on your file system, under a location named according to the following scheme: `target/deviations/<year>/<zoom>/<x>-<y>.png`.

### Sixth Milestone
<p>
This milestone consists in implementing an interactive user interface so that users can select which data set (either the temperatures or the deviations) as well as which year they want to observe. 
You will have to complete the file `Interaction2.scala`. 
</p>

<p>
This milestone introduces the concept of Layers. 
A Layer describes the additional information shown on a map. 
In your case, you will have two layers: one showing the temperatures over time, and one showing the temperature deviations over time.
</p>

<p>
The user interface implementation will use your availableLayers to build buttons allowing to choose which layer to enable.
Each layer has a name, a color scale and a range of supported years. 
The value over time of the enabled layer is represented by a Signal[Layer] value.
</p>