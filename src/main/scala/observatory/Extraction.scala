package observatory

import java.io.InputStream
import java.time.LocalDate

import org.apache.spark.sql.{DataFrame, Row}
import org.apache.spark.sql.types.{DoubleType, IntegerType, ShortType, StringType, StructField, StructType}
import org.apache.spark.sql.types.Metadata
import org.apache.spark.rdd.RDD
import scala.io.Source
import java.nio.file.Paths

/**
  * 1st milestone: data extraction
  */
object Extraction extends ExtractionInterface {

  import org.apache.spark.sql.SparkSession

  lazy val spark: SparkSession =
    SparkSession
      .builder()
      .appName("Data Extraction")
      .master("local")
      .getOrCreate()

  import spark.implicits._

  /**
    *
    * @param stationsFile
    * @return
    */
  def parseStations(stationsFile: String): DataFrame = {
    val stationsSchema = StructType(Array(
      StructField("STN", IntegerType, nullable = true),
      StructField("WBAN", IntegerType, nullable = true),
      StructField("Latitude", DoubleType, nullable = true),
      StructField("Longitude", DoubleType, nullable = true)
    ))
    val toInt = (s: String) => s.toInt
    val toDouble = (s: String) => s.toDouble
    val fileStream = getClass.getResourceAsStream(stationsFile)
    val strSeq = Source.fromInputStream(fileStream).getLines.toSeq
    val dataRDD: RDD[String] = spark.sparkContext.parallelize(strSeq)
    val rowRDD = dataRDD
      .map(_.split(",", -1))
      .map(_.map(x => if (x == "") null else x))
      .map(attributes => Row(
        parse(attributes(0), toInt),
        parse(attributes(1), toInt),
        parse(attributes(2), toDouble),
        parse(attributes(3), toDouble)))
    val res = spark.createDataFrame(rowRDD, stationsSchema)
    res
  }

  private def parse(attr: String, f: String => Any): Any = Option(attr).map(f).orNull

  /**
    *
    * @param temperaturesFile
    * @return
    */
  def parseTemperatures(temperaturesFile: String): DataFrame = {
    val temperaturesSchema = StructType(Array(
      StructField("STN", IntegerType, true),
      StructField("WBAN", IntegerType, true),
      StructField("Month", IntegerType, true),
      StructField("Day", IntegerType, true),
      StructField("Temperature", DoubleType, true)
    ))
    val toInt = (s: String) => s.toInt
    val toDouble = (s: String) => s.toDouble
    val fileStream = getClass.getResourceAsStream(temperaturesFile)
    val strSeq = Source.fromInputStream(fileStream).getLines.toSeq
    val dataRDD: RDD[String] = spark.sparkContext.parallelize(strSeq)
    val rowRDD = dataRDD
      .map(_.split(",", -1))
      .map(_.map(x => if (x == "") null else x))
      .map(attributes => Row(
        parse(attributes(0), toInt),
        parse(attributes(1), toInt),
        parse(attributes(2), toInt),
        parse(attributes(3), toInt),
        parse(attributes(4), toDouble)))
    val temperaturesDf = spark.createDataFrame(rowRDD, temperaturesSchema)
    temperaturesDf
  }

  /**
    *
    * @param tempDf
    * @param staDf
    * @return
    */
  def convertToRDD(tempDf: DataFrame, staDf: DataFrame): RDD[Row] = {
    val genDf = staDf.join(tempDf, staDf("STN") <=> tempDf("STN") && staDf("WBAN") <=> tempDf("WBAN"))
    val filtDf = genDf.na.drop(Seq("Latitude", "Longitude"))
    val newDf = filtDf.select("Month", "Day", "Latitude", "Longitude", "Temperature")
    val res: RDD[Row] = newDf.rdd
    res
  }

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Year, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Temperature)] = { // create stationsDF from RDD
    val tempDF = parseTemperatures(temperaturesFile)
    val staDF = parseStations(stationsFile)
    val interRDD = convertToRDD(tempDF, staDF)
    val resRDD = interRDD.map {
      case Row(month: Int, day: Int, lat: Double, long: Double, temp: Temperature) => (LocalDate.of(year, month, day), Location(lat,long), (temp-32)/1.8)
    }.collect()
    //spark.stop()
    resRDD
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Temperature)]): Iterable[(Location, Temperature)] = {
      val recordsRDD = spark.sparkContext.parallelize(records.toList)
      val recordsDF = recordsRDD.map(i => (i._1.getYear(), i._2, i._3)).toDF("Year", "Location", "Temperature") //records.toSeq.
      val resDF: DataFrame = recordsDF.groupBy($"Location", $"Year")
                                      .agg("Temperature" -> "avg").as("avgTemperature")
                                      .select($"Location", $"avg(Temperature)")
    resDF.map(row => (Location(row.getStruct(0).getDouble(0), row.getStruct(0).getDouble(1)), row.getDouble(1)))
         .collect()
  }

  //spark.stop()
  object parseStations

}



