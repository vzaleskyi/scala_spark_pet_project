package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import Math._

/**
  * 3rd milestone: interactive visualization
  */
object Interaction extends InteractionInterface {

  /**
    * @param tile Tile coordinates
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(tile: Tile): Location = {
    val lon = tile.x.toDouble / (1 << tile.zoom) * 360.0 - 180.0 // 1 << n equal to 2 ^ n
    val lat = toDegrees(atan(sinh(PI * (1.0 - 2.0 * tile.y.toDouble / (1 << tile.zoom)))))
    Location(lat, lon)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors       Color scale
    * @param tile         Tile coordinates
    * @return A 256×256 image showing the contents of the given tile
    */
  def tile(
            temperatures: Iterable[(Location, Temperature)],
            colors: Iterable[(Temperature, Color)],
            tile: Tile
          ): Image = {
    val size = 256
    val alpha = 127
    val higherZoom = 8 // 2⁸ * 2⁸ (256 = 2⁸) corresponds to zoom level 8

    val pixels = (for {
      i <- (0 until size).par
      j <- 0 until size
      subY = size * tile.y + i
      subX = size * tile.x + j
      subtile = Tile(subX, subY, tile.zoom + higherZoom)

      loc = tileLocation(subtile)
      predictedTemp = Visualization.predictTemperature(temperatures, loc)
      color = Visualization.interpolateColor(colors, predictedTemp)
      pixel = Pixel(color.red, color.green, color.blue, alpha)
    } yield pixel).toArray

    Image(size, size, pixels)
  }

  def genZoomXY(z: Int) = {
    (for {
      x <- (0 until 1 << z)
      y <- (0 until 1 << z)
    } yield (x, y, z))
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    *
    * @param yearlyData    Sequence of (year, data), where `data` is some data associated with
    *                      `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](
                           yearlyData: Iterable[(Year, Data)],
                           generateImage: (Year, Tile, Data) => Unit
                         ): Unit = {
    for {
      z <- 0 to 3
      xyz <- genZoomXY(z)
      (x, y, zoom) = (xyz._1, xyz._2, xyz._3)
      tile = Tile(x, y, zoom)
      (year, data) <- yearlyData
    } generateImage(year, tile, data)
  }
}

