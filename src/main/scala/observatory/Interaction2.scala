package observatory

import observatory.LayerName.{Deviations, Temperatures}

/**
  * 6th (and last) milestone: user interface polishing
  */
object Interaction2 extends Interaction2Interface {

  /**
    * constructs two layers: one showing the temperatures over time, and one showing the temperature deviations over time.
    * @return The available layers of the application
    */
  def availableLayers: Seq[Layer] = {
    val colorScale = List(
      (7.0, Color(0, 0, 0)),
      (4.0, Color(255, 0, 0)),
      (2.0, Color(255, 255, 0)),
      (0.0, Color(255, 255, 255)),
      (-2.0, Color(0, 255, 255)),
      (-7.0, Color(0, 0, 255)),
    )
    val tempLayer = Layer(Temperatures, colorScale,1975 to 2015)
    val devLayer = Layer(Deviations, colorScale, 1991 to 2015)
    List(tempLayer, devLayer)
  }

  /**
    * @param selectedLayer A signal carrying the layer selected by the user
    * @return A signal containing the year bounds corresponding to the selected layer
    */
  def yearBounds(selectedLayer: Signal[Layer]): Signal[Range] = {
    Signal(selectedLayer().bounds)
  }

  /**
    * @param selectedLayer The selected layer
    * @param sliderValue The value of the year slider
    * @return The value of the selected year, so that it never goes out of the layer bounds.
    *         If the value of `sliderValue` is out of the `selectedLayer` bounds,
    *         this method should return the closest value that is included
    *         in the `selectedLayer` bounds.
    */
  def yearSelection(selectedLayer: Signal[Layer], sliderValue: Signal[Year]): Signal[Year] = {
    val bounds: Signal[Range] = yearBounds(selectedLayer)
    val max: Signal[Year] = Signal(bounds().last)
    val min = Signal(bounds().head)

    val res = bounds().contains(sliderValue()) match {
      case false => if (sliderValue().intValue() < min().intValue()) min else max
      case true => sliderValue
    }
    res
  }

  /**
    * @param selectedLayer The selected layer
    * @param selectedYear The selected year
    * @return The URL pattern to retrieve tiles
    */
  def layerUrlPattern(selectedLayer: Signal[Layer], selectedYear: Signal[Year]): Signal[String] = {
    val lName: String = selectedLayer().layerName.id
    val year = selectedYear()
    val res = Signal(s"target/$lName/$year/{zoom}/{x}-{y}.png")
    res
  }

  /**
    * @param selectedLayer The selected layer
    * @param selectedYear The selected year
    * @return The caption to show
    */
  def caption(selectedLayer: Signal[Layer], selectedYear: Signal[Year]): Signal[String] = {
    val lName = selectedLayer().layerName.toString.capitalize
    val year = selectedYear().intValue()
    Signal(f"$lName%s ($year%s)")
  }

}

// Interface used by the grading infrastructure. Do not change signatures
// or your submission will fail with a NoSuchMethodError.
trait Interaction2Interface {
  def availableLayers: Seq[Layer]
  def yearBounds(selectedLayer: Signal[Layer]): Signal[Range]
  def yearSelection(selectedLayer: Signal[Layer], sliderValue: Signal[Year]): Signal[Year]
  def layerUrlPattern(selectedLayer: Signal[Layer], selectedYear: Signal[Year]): Signal[String]
  def caption(selectedLayer: Signal[Layer], selectedYear: Signal[Year]): Signal[String]
}

sealed abstract class LayerName(val id: String)
object LayerName {
  case object Temperatures extends LayerName("temperatures")
  case object Deviations extends LayerName("deviations")
}

/**
  * @param layerName Name of the layer
  * @param colorScale Color scale used by the layer
  * @param bounds Minimum and maximum year supported by the layer
  */
case class Layer(layerName: LayerName, colorScale: Seq[(Temperature, Color)], bounds: Range)

