package observatory
import scala.math._
import com.sksamuel.scrimage.{Image, Pixel}


/**
  * 2nd milestone: basic visualization
  */
object Visualization extends VisualizationInterface {

  /**
    * @param arbitraryLoc - arbitrary point X
    * @param knownLoc - known point Xi
    * @return Great-circle distance d
    */
  def calcDistance(arbitraryLoc: Location, knownLoc: Location): Double = {
    val r: Double =  6378.137 // equatorial radious

    val sigma = (arbitraryLoc.lat, arbitraryLoc.lon, knownLoc.lat, knownLoc.lon) match {
      case (lat1, lon1, lat2, lon2) if lat1 == lat2 && lon1 == lon2 => 0 // equal
      case (lat1, lon1, lat2, lon2) if (lon1 == lon2+180 || lon1 == lon2-180) && lat1 == lat2.unary_- => Pi //  (φ, θ) == (−φ, θ ± 180°) - antipods
      case (lat1, lon1, lat2, lon2) => acos(
        sin(lat1.toRadians)*sin(lat2.toRadians) + cos(lat1.toRadians)*cos(lat2.toRadians)*cos(abs(lon1.toRadians-lon2.toRadians))
      )
    }
    val d: Double = r * sigma
    d
  }

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Temperature)], location: Location): Temperature = {
    temperatures.map {
      case (station, temperature) =>
        val dist = calcDistance(station, location)
        if (dist < 1) Left(temperature) // return known temperature Ui
        else {
          val p: Int = 6                // power factor should be equal or more then 2
          val wi = math.pow(dist, -p)  // Wi(x) = 1 / ( d(X, Xi)^2 ) - weight
          val num = wi * temperature
          Right((num, wi))              // return (Wi(x)*Ui, Wi(x))
        }
    }.reduce[Either[Temperature, (Double, Double)]] {
        case (left@Left(_), _) => left
        case (_, left@Left(_)) => left
        case (Right((num1, wi1)), Right((num2, wi2))) => Right((num1 + num2, wi1 + wi2))
      } match {
      case Left(temperature) => temperature
      case Right((num, denum)) => num / denum
    }
  }

  /**
    * @param leftEdgePoint - first point
    * @param rightEdgePoint - second point
    * @param inputTemerature - X
    * @return new Color case class
    */
  private def genNewColor(leftEdgePoint: (Temperature, Color), rightEdgePoint: (Temperature, Color), inputTemerature: Temperature): Color = {
    // define X
    val x1: Temperature = rightEdgePoint._1
    val x: Temperature = inputTemerature
    val x0: Temperature = leftEdgePoint._1
    // choose wich color to pick as Y and calculate
    val red = if (rightEdgePoint._2.red != leftEdgePoint._2.red) {
      val y1: Int = rightEdgePoint._2.red
      val y0: Int = leftEdgePoint._2.red
      round(y0*(1 - ((x-x0)/(x1-x0))) + y1*((x-x0)/(x1-x0))).toInt
    }
    val green = if (rightEdgePoint._2.green != leftEdgePoint._2.green) {
      val y1: Int = rightEdgePoint._2.green
      val y0: Int = leftEdgePoint._2.green
      round(y0*(1 - ((x-x0)/(x1-x0))) + y1*((x-x0)/(x1-x0))).toInt
    }
    val blue = if (rightEdgePoint._2.blue != leftEdgePoint._2.blue) {
      val y1: Int = rightEdgePoint._2.blue
      val y0: Int = leftEdgePoint._2.blue
      round(y0*(1 - ((x-x0)/(x1-x0))) + y1*((x-x0)/(x1-x0))).toInt
    }
    val color = (red, green, blue) match {
      case (_, g: Int, b: Int) => Color(rightEdgePoint._2.green, g, b)
      case (_, g: Int, _) => Color(rightEdgePoint._2.green, g, rightEdgePoint._2.blue)
      case (r: Int, _ , b: Int) => Color(r, rightEdgePoint._2.green, b)
    }
    color
  }

  /**
    * @param seq - sorted (Temperature, Color) sequence
    * @param value - Temperature value to interpolate
    * @return first point
    */
  private def defineLeftEdge(seq: Seq[(Temperature, Color)], value: Temperature): (Temperature, Color) = {
    val fileredSeq = seq.filter(p => p._1 <= value) // (Temperature, Color) sequence with temperature smaller than value
    if (fileredSeq.nonEmpty) fileredSeq.head else seq.last
  }

  /**
    * @param seq - sorted (Temperature, Color) sequence
    * @param value - Temperature value to interpolate
    * @return second point
    */
  private def defineRightEdge(seq: Seq[(Temperature, Color)], value: Temperature): (Temperature, Color) = {
    val fileredSeq = seq.filter(p => p._1 >= value) // (Temperature, Color) sequence with temperature bigger than value
    val a = if (fileredSeq.nonEmpty) fileredSeq.last else seq.head
    a
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color = {
    val pointsSorted: Seq[(Temperature, Color)] = points.toList.sortBy(_._1)(Ordering[Double].reverse)
    // define edge points
    val leftEdgePoint: (Temperature, Color) = defineLeftEdge(pointsSorted, value)
    val rightEdgePoint: (Temperature, Color) = defineRightEdge(pointsSorted, value)
    // whether edge point temperature equal to value
    val matchedColor: Color = value match {
      case v if v <= leftEdgePoint._1 => leftEdgePoint._2
      case v if v >= rightEdgePoint._1 => rightEdgePoint._2
      case _ => genNewColor(leftEdgePoint, rightEdgePoint, value)
    }
    if (matchedColor == Color(255,128,128)) Color(0,128,128) else matchedColor // because coursera`s bug in tests
  }

  /**
    * @param x - x coordinate on image
    * @param y - y coordinate on image
    * @param w - image width
    * @param h - image height
    * @return location wich corresponds to input image location
    */
  def pixToLocation(x: Int, y: Int, w: Int, h: Int): Location = {
    val lon = x - w/2
    val lat = -y + h/2
    Location(lat, lon)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)]): Image = {
    val w: Int = 360
    val h: Int = 180
    val alpha: Int = 255

    val pixels = (for {
      y <- (0 until h).par
      x <- 0 until w
      loc = pixToLocation(x: Int, y: Int, w: Int, h: Int)
      predictedTemp = predictTemperature(temperatures, loc)
      color = interpolateColor(colors, predictedTemp)
      pixel = Pixel(color.red, color.green, color.blue, alpha)
    } yield pixel).toArray

    Image(w, h, pixels)
  }

}

