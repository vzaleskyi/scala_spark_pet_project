package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import observatory.Interaction.tileLocation

/**
  * 5th milestone: value-added information visualization
  */
object Visualization2 extends Visualization2Interface {

  /**
    * @param point (x, y) coordinates of a point in the grid cell
    * @param d00 Top-left value
    * @param d01 Bottom-left value
    * @param d10 Top-right value
    * @param d11 Bottom-right value
    * @return A guess of the value at (x, y) based on the four known values, using bilinear interpolation
    *         See https://en.wikipedia.org/wiki/Bilinear_interpolation#Unit_Square
    */
  def bilinearInterpolation(
    point: CellPoint,
    d00: Temperature,
    d01: Temperature,
    d10: Temperature,
    d11: Temperature
  ): Temperature = {
    val b1 = d00
    val b2 = d10 - d00
    val b3 = d01 - d00
    val b4 = d00 - d10 - d01 + d11
    b1 + b2*point.x + b3*point.y + b4*point.x*point.y
  }

  /**
    * @param loc point location
    * @param grid Grid to visualize
    * @return predicted Temperature
    */
  def getTemperature(loc: Location, grid: GridLocation => Temperature) : Temperature = {
    val cellPoint = CellPoint(loc.lon - loc.lon.toInt,loc.lat - loc.lat.toInt) // point coordinates into the cell
    val lat = loc.lat.toInt                                                    // calculating cell edge points
    val lon = loc.lon.toInt
    val d00 = GridLocation(lat, lon)
    val d01 = GridLocation(lat+1, lon)
    val d10 = GridLocation(lat, lon+1)
    val d11 = GridLocation(lat+1, lon+1)
    bilinearInterpolation(cellPoint, grid(d00),grid(d01), grid(d10),grid(d11))
  }

  /**
    * @param grid Grid to visualize
    * @param colors Color scale to use
    * @param tile Tile coordinates to visualize
    * @return The image of the tile at (x, y, zoom) showing the grid using the given color scale
    */
  def visualizeGrid(
    grid: GridLocation => Temperature,
    colors: Iterable[(Temperature, Color)],
    tile: Tile
  ): Image = {
    val size = 256
    val alpha = 127
    val higherZoom = 8 // 2⁸ * 2⁸ (256 = 2⁸) corresponds to zoom level 8

    val pixels = (for {
      i <- (0 until size).par
      j <- 0 until size

      subY = size * tile.y + i
      subX = size * tile.x + j
      subtile = Tile(subX, subY, tile.zoom + higherZoom)
      loc = tileLocation(subtile)

      temp = getTemperature(loc, grid)
      color = Visualization.interpolateColor(colors, temp)

      pixel = Pixel(color.red, color.green, color.blue, alpha)
    } yield pixel).toArray

    Image(size, size, pixels)
  }

}
