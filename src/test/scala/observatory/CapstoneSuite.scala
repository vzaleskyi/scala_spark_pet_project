package observatory
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CapstoneSuite extends VisualizationTest
  with InteractionTest
  with Interaction2Test
//  with Visualization2Test
//  with ManipulationTest
