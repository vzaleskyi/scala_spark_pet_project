package observatory

import observatory.Interaction2.{yearSelection, layerUrlPattern}
import observatory.LayerName.{Deviations, Temperatures}
import org.scalatest.FunSuite
import org.scalatest.prop.Checkers

trait Interaction2Test extends FunSuite with Checkers {
  val colorScale = List(
    (7.0, Color(0, 0, 0)),
    (4.0, Color(255, 0, 0)),
    (2.0, Color(255, 255, 0)),
    (0.0, Color(255, 255, 255)),
    (-2.0, Color(0, 255, 255)),
    (-7.0, Color(0, 0, 255)),
  )
  val tempLayer = Signal(Layer(Temperatures, colorScale, 1975 to 2015))
  val devLayer = Signal(Layer(Deviations, colorScale, 1991 to 2015))


  test("out of left boundary for temperature layer") {
    val res = yearSelection(tempLayer, Signal(1970))
    assertResult(Signal(1975)().intValue())(res().intValue())
  }

  test("within boundaries for temperature layer") {
    val res = yearSelection(tempLayer, Signal(2000))
    assertResult(Signal(2000)().intValue())(res().intValue())
  }

  test("out of right boundary for temperature layer") {
    val res = yearSelection(tempLayer, Signal(2030))
    assertResult(Signal(2015)().intValue())(res().intValue())
  }

  test("url pattern test case with temperature layer name") {
    val res: Signal[String]= layerUrlPattern(devLayer, Signal(2015))
    assertResult(Signal("target/deviations/2015/{zoom}/{x}-{y}.png").apply())(res.apply())
  }
}
