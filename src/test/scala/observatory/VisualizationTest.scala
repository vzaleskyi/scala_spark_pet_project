package observatory


import org.scalatest.FunSuite
import org.scalatest.prop.Checkers

trait VisualizationTest extends FunSuite with Checkers {
  val referenceColors: Seq[(Temperature, Color)] = List(
    (60.0,Color(255,255,255)),
    (32.0,Color(255,0,0)),
    (12.0,Color(255,255,0)),
    (0.0,Color(0,255,255)),
    (-15.0,Color(0,0,255)),
    (-27.0,Color(255,0,255)),
    (-50.0,Color(33,0,107)),
    (-60.0,Color(0,0,0))
  )

  val (w, h) = (360, 180)

  test("Above max temperature"){
    val res = Visualization.interpolateColor(referenceColors, 65.0);
    assertResult(Color(255,255,255))(res)
  }

  test("Above eq max temperature"){
    val res = Visualization.interpolateColor(referenceColors, 60.0);
    assertResult(Color(255,255,255))(res)
  }

  test("Eq 32"){
    val res = Visualization.interpolateColor(referenceColors, 32.0);
    assertResult(Color(255,0,0))(res)
  }

  test("Eq 12"){
    val res = Visualization.interpolateColor(referenceColors, 12.0);
    assertResult(Color(255,255,0))(res)
  }

  test("Eq 0"){
    val res = Visualization.interpolateColor(referenceColors, 0.0);
    assertResult(Color(0,255,255))(res)
  }

  test("Eq -15"){
    val res = Visualization.interpolateColor(referenceColors, -15.0);
    assertResult(Color(0,0,255))(res)
  }

  test("Eq -27"){
    val res = Visualization.interpolateColor(referenceColors, -27.0);
    assertResult(Color(255,0,255))(res)
  }

  test("Eq -50"){
    val res = Visualization.interpolateColor(referenceColors, -50.0);
    assertResult(Color(33,0,107))(res)
  }

  test("Below min temperature"){
    val res = Visualization.interpolateColor(referenceColors, -65.0);
    assertResult(Color(0,0,0))(res)
  }

  test("Below eq min temperature"){
    val res = Visualization.interpolateColor(referenceColors, -60.0);
    assertResult(Color(0,0,0))(res)
  }

  test("Mid temperature"){
    val res = Visualization.interpolateColor(referenceColors, 6.0);
    assertResult(Color(128,255,128))(res)
  }

  test("Predict by eq"){
    val data = Seq(
      (Location(10,10),25.0),
      (Location(20,20),15.0),
      (Location(10.5,10),5.0)
    )
    val predict = Visualization.predictTemperature(data,Location(20,20))
    assertResult(15.0)(predict)
  }

  test("Predict avg"){
    val data = Seq(
      (Location(10,10),25.0),
      (Location(10.2,10),23.0),
      (Location(10.5,10),24.0)
    )
    val predict = Visualization.predictTemperature(data,Location(10.1,10.2))
    assert(predict > 23 && predict < 25)
  }

  test("test 1 quadrants lon-lat"){
    val res = Visualization.pixToLocation(220, 45, w, h)
    assertResult(Location(45, 40))(res)
  }

  test("test 2 quadrants lon-lat"){
    val res = Visualization.pixToLocation(40, 30, w, h)
    assertResult(Location(60, -140))(res)
  }

  test("test 3 quadrants lon-lat"){
    val res = Visualization.pixToLocation(140, 135, w, h)
    assertResult(Location(-45, -40))(res)
  }

  test("test 4 quadrants lon-lat"){
    val res = Visualization.pixToLocation(320, 150, w, h)
    assertResult(Location(-60, 140))(res)
  }
}